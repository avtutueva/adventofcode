file = open('camp_sections.txt', 'r')

pair_sections = file.read().splitlines()

# In how many assignment pairs does one range fully contain the other?

counter = 0

for pair in pair_sections:
    limits = []
    for i in pair.split(','): # create list with sections limits
        [limits.append(int(j)) for j in i.split('-')]
    # count ranges fully contained other
    if (((limits[0] <= limits[2]) & (limits[1] >= limits[3])) | ((limits[2] <= limits[0]) & (limits[3] >= limits[1]))):
        counter += 1

print(counter)


# In how many assignment pairs do the ranges overlap?
counter = 0

for pair in pair_sections:
    limits = []
    for i in pair.split(','): # create list with sections limits
        [limits.append(int(j)) for j in i.split('-')]
    # count zero intersections
    if ((limits[1] < limits[2]) | (limits[3] < limits[0])):
        counter += 1

print(len(pair_sections) - counter)