file = open('rucksacks.txt', 'r')
rucksacks = file.read().splitlines()

# Task 1: Find the item type that appears in both compartments of each rucksack. What is the sum of the priorities of those item types?

priority_total = 0

for i in rucksacks: # for each rucksack
    for c in i[:int(len(i) / 2)]: # for each letter from the first compartment find the match in the second compartment
        if c in i[int(len(i) / 2):]: # if there is match, calculate priority
            if (c.islower()):  # if letter is lowercase then subtract 96
                priority_total += ord(c) - 96
            else:  # else subtract 38
                priority_total += ord(c) - 38
            break

# print result
print(priority_total)


# Task 2: Find the item type that corresponds to the badges of each three-Elf group. What is the sum of the priorities of those item types?

priority_total = 0

for i in range(0, len(rucksacks), 3): # for each group of three
    for c in rucksacks[i]: # for each letter from the first rucksack
        if (c in rucksacks[i + 1]) & (c in rucksacks[i + 2]): # if this letter there is in other rucksacks in group, calculate priority
            if (c.islower()):  # if letter is lowercase then subtract 96
                priority_total += ord(c) - 96
            else:  # else subtract 38
                priority_total += ord(c) - 38
            break

print(priority_total)