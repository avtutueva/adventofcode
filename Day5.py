file = open('crates.txt', 'r')

lines = file.read().splitlines()

puzzle = []

for i in range(len(lines)): # read puzzle
    if lines[i] == '': # break in the case of empty string (end of puzzle)
        break
    puzzle.insert(0, lines[i])

del puzzle[0] # del row with numbers

k = 0
stacks = [] # stacks info
while k < len(puzzle[0]): # go through the columns
    temp = ''
    for j in range(len(puzzle)): # go through the rows
        temp += puzzle[j][k: k+3]
    k += 4
    temp = temp.replace('[', '').replace(']', '').strip()
    stacks.append(temp)

# After the rearrangement procedure completes, what crate ends up on top of each stack? (both parts)

stacks_second_part = stacks.copy()
print(stacks)

i += 1 # skip empty string
while i < len(lines):
    commands = [int(c) for c in lines[i].replace('move ', '').replace('from ', '').replace('to ', '').split(' ')] # parse command

    stacks_second_part[commands[2] - 1] += stacks_second_part[commands[1] - 1][-commands[0]:]
    stacks_second_part[commands[1] - 1] = stacks_second_part[commands[1] - 1][:-commands[0]]


    while commands[0] > 0: # move all crates between stacks
        stacks[commands[2] - 1] += stacks[commands[1] - 1][-1]
        stacks[commands[1] - 1] = stacks[commands[1] - 1][:-1]
        commands[0] = commands[0] - 1
    i += 1

for i in stacks: # print result for the first task
    print(i[-1], end='')

print()

for i in stacks_second_part: # print result for the second task
    print(i[-1], end='')
