file = open('RockPaperScissors.txt', 'r')
lines = file.read().splitlines()

enemy_list = ['A', 'B', 'C']
your_list = ['X', 'Y', 'Z']

# first part
score = 0
for i in lines:
    answers = i.split(' ')
    enemy_index = enemy_list.index(answers[0]) + 1
    your_index = your_list.index(answers[1]) + 1

    if enemy_index == your_index:
        score += 3
    elif ((enemy_index - your_index) == -1) or ((enemy_index - your_index) == 2):
        score += 6

    score += your_index

print(score)


# second part
score = 0
for i in lines:
    answers = i.split(' ')
    enemy_index = enemy_list.index(answers[0])

    if answers[1] == 'Y':
        score += 3
        score += (enemy_index + 1)
    elif answers[1] == 'X':
        score += 1
        if answers[0] == 'A':
            score += 2
        elif answers[0] == 'C':
            score += 1
    else:
        score += 7
        if answers[0] == 'A':
            score += 1
        elif answers[0] == 'B':
            score += 2

print(score)

