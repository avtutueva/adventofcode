str = open('code.txt', 'r').read()

# str = 'bvwbjplbgvbhsrlpgdmjqwftvncz'

# How many characters need to be processed before the first start-of-packet marker is detected?

length = 14 # 4 or 14

i = 0
result = i
while (i + length) <= len(str):
    temp = str[i: i + length] # substring with supposed code
    is_equal = False # flag

    for j in range(0, length - 1):
        for k in range(j + 1, length):
            if temp[j] == temp[k]:
                is_equal = True
                break
        if is_equal:
            break
    else:
        result = i + length
        break
    i += 1

print(result)