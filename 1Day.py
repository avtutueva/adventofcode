file = open('calories.txt', 'r')

lines = file.readlines()

total = 0
elves_calories = []

for i in lines:
    if i == '\n':
        elves_calories.append(int(total))
        total = 0
    else:
        total += int(i)

print(max(elves_calories))
print(sum(sorted(elves_calories, reverse=True)[:3]))
